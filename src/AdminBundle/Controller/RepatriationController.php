<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Repatriation;
use AdminBundle\Form\RepatriationType;

/**
 * Repatriation controller.
 *
 * @Route("/repatriation")
 */
class RepatriationController extends Controller
{

    /**
     * Lists all Repatriation entities.
     *
     * @Route("/", name="admin_repatriation")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $count = 10;
        $page = max($request->get('page'), 1);

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdminBundle:Repatriation')->findBy(
            [],
            ['id' => 'desc'],
            $count,
            ($count * ($page - 1))
        );

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all Repatriation entities.
     *
     * @Route("/upload", name="admin_repatriation_upload")
     * @Method("POST")
     */
    public function uploadCSVAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($request->files as $file) {
            $name = $file->getClientOriginalName();
            $uploadDir = $this->getParameter('kernel.root_dir') . "/../web/up/csv/";

            if ($file->move($uploadDir, $name)) {
                $fh = fopen($uploadDir . $name, 'r');

                while (($data = fgetcsv($fh, 1000, ",")) !== false) {

                    if (strlen($data[0]) == strlen((int)$data[0])) {
                        $name = explode(" ", trim($data[1]));
                        if ($data[9]) {
                            $familyCount = $data[9];
                        }


                        $lastName = $name[0];
                        $firstName = $name[1];
                        if (count($name) == 3) {
                            $secondName = $name[2];
                        } else {
                            $secondName = "";
                        }
                        $entity = new Repatriation();
                        $entity->setFromCountry($request->get('country'));
                        $entity->setPage($data[0]);
                        $entity->setFirstName($firstName);
                        $entity->setSecondName($secondName);
                        $entity->setLastName($lastName);
                        $entity->setFamilyStatus($data[2]);
                        $entity->setBirthYear($data[3]);
                        $entity->setInYear($data[4]);
                        $entity->setShip($data[5]);
                        $entity->setFromCity($data[6]);
                        $entity->setProfession($data[7]);
                        $entity->setToPlace($data[8]);
                        $entity->setFamilyMemberCount($familyCount);
                        $entity->setOther($data[10]);

                        $em->persist($entity);
                    }

                }
            }

            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_repatriation'));

    }


    /**
     * Creates a new Repatriation entity.
     *
     * @Route("/", name="admin_repatriation_create")
     * @Method("POST")
     * @Template("AdminBundle:Repatriation:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Repatriation();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_repatriation_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Repatriation entity.
     *
     * @param Repatriation $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Repatriation $entity)
    {
        $form = $this->createForm(
            new RepatriationType(),
            $entity,
            array(
                'action' => $this->generateUrl('admin_repatriation_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Repatriation entity.
     *
     * @Route("/new", name="admin_repatriation_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Repatriation();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Repatriation entity.
     *
     * @Route("/{id}", name="admin_repatriation_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Repatriation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Repatriation entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Repatriation entity.
     *
     * @Route("/{id}/edit", name="admin_repatriation_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Repatriation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Repatriation entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Repatriation entity.
     *
     * @param Repatriation $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Repatriation $entity)
    {
        $form = $this->createForm(
            new RepatriationType(),
            $entity,
            array(
                'action' => $this->generateUrl('admin_repatriation_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Repatriation entity.
     *
     * @Route("/{id}", name="admin_repatriation_update")
     * @Method("PUT")
     * @Template("AdminBundle:Repatriation:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Repatriation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Repatriation entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_repatriation_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Repatriation entity.
     *
     * @Route("/{id}", name="admin_repatriation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Repatriation')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Repatriation entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_repatriation'));
    }

    /**
     * Creates a form to delete a Repatriation entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_repatriation_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
}
