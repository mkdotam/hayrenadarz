<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Newspaper;
use AdminBundle\Form\NewspaperType;

/**
 * Newspaper controller.
 *
 * @Route("/newspaper")
 */
class NewspaperController extends Controller
{

    /**
     * Lists all Newspaper entities.
     *
     * @Route("/", name="admin_newspaper")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $count = 10;
        $page = max($request->get('page'), 1);

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdminBundle:Newspaper')->findBy([], ['id' => 'desc'], $count, ($count * ($page - 1)));

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Newspaper entity.
     *
     * @Route("/", name="admin_newspaper_create")
     * @Method("POST")
     * @Template("AdminBundle:Newspaper:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Newspaper();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (is_object($entity->getImage()) &&
                get_class($entity->getImage()) == 'Symfony\Component\HttpFoundation\File\UploadedFile') {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadableManager->markEntityToUpload($entity, $entity->getImage());
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_newspaper_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Newspaper entity.
     *
     * @param Newspaper $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Newspaper $entity)
    {
        $form = $this->createForm(new NewspaperType(), $entity, array(
            'action' => $this->generateUrl('admin_newspaper_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Newspaper entity.
     *
     * @Route("/new", name="admin_newspaper_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Newspaper();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Newspaper entity.
     *
     * @Route("/{id}", name="admin_newspaper_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Newspaper')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newspaper entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Newspaper entity.
     *
     * @Route("/{id}/edit", name="admin_newspaper_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Newspaper')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newspaper entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Newspaper entity.
    *
    * @param Newspaper $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Newspaper $entity)
    {
        $form = $this->createForm(new NewspaperType(), $entity, array(
            'action' => $this->generateUrl('admin_newspaper_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing Newspaper entity.
     *
     * @Route("/{id}", name="admin_newspaper_update")
     * @Method("PUT")
     * @Template("AdminBundle:Newspaper:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Newspaper')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newspaper entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if (is_object($entity->getImage()) &&
                get_class($entity->getImage()) == 'Symfony\Component\HttpFoundation\File\UploadedFile') {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadableManager->markEntityToUpload($entity, $entity->getImage());
            }
            $em->flush();

            return $this->redirect($this->generateUrl('admin_newspaper_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Newspaper entity.
     *
     * @Route("/{id}", name="admin_newspaper_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Newspaper')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Newspaper entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_newspaper'));
    }

    /**
     * Creates a form to delete a Newspaper entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_newspaper_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
