<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="admin_home")
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig');
    }


    /**
     * @Route("/upload/image", name="admin_image_upload")
     * @Method("POST")
     */
    public function uploadImageAction(Request $request)
    {
        $webPath = '/i/up/';
        $uploadDir = $this->get('kernel')->getRootDir() . '/../web' . $webPath;
        $result = array();

        foreach ($request->files as $file) {
            if (in_array(
                strtolower($file->getClientMimeType()),
                array('image/png', 'image/jpg', 'image/gif', 'image/jpeg')
            )) {

                $filename = $file->getClientOriginalName();
                $file->move($uploadDir, $filename);

            }

        }
        $result = $webPath . $filename;

        $response = new Response($result);
        $response->headers->set('Content-Type', "text/html; charset=utf-8");

        return $response;
    }

    /**
     * @Route("/upload/file", name="admin_file_upload")
     * @Method("POST")
     */
    public function uploadFileAction(Request $request)
    {
        $webPath = '/i/file/';
        $uploadDir = $this->get('kernel')->getRootDir() . '/../web' . $webPath;
        $result = array();

        foreach ($request->files as $file) {
            // if (in_array(strtolower($file->getClientMimeType()),
            // array('image/png', 'image/jpg', 'image/gif', 'image/jpeg'))) {

            $filename = $file->getClientOriginalName();
            $file->move($uploadDir, $filename);

            // }

            $result = array(
                'filelink' => $webPath . $filename,
                'filename' => $filename
            );
        }

        $response = new Response(stripslashes(json_encode($result)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    /**
     * @Route("/pagination/{path}/{page}", name="admin_pagination")
     */
    public function paginationAction($path, $page, $params = array())
    {
        $entity = ucfirst(substr($path, strpos($path, '_') + 1));

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $total = $qb->select('count(t.id)')->from('AdminBundle:' . $entity, 't')->getQuery()->getSingleScalarResult();
        $pages = ceil($total / 10);

        return $this->render(
            'AdminBundle:Default:pagination.html.twig',
            [
                'path'   => $path,
                'pages'  => $pages,
                'page'   => (int)$page,
                'params' => $params,
            ]);
    }

}
