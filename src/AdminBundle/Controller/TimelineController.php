<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Timeline;
use AdminBundle\Form\TimelineType;

/**
 * Timeline controller.
 *
 * @Route("/timeline")
 */
class TimelineController extends Controller
{

    /**
     * Lists all Timeline entities.
     *
     * @Route("/", name="admin_timeline")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $count = 10;
        $page = max($request->get('page'), 1);

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdminBundle:Timeline')->findBy([], ['id' => 'desc'], $count, ($count * ($page - 1)));

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Timeline entity.
     *
     * @Route("/", name="admin_timeline_create")
     * @Method("POST")
     * @Template("AdminBundle:Timeline:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Timeline();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_timeline_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Timeline entity.
     *
     * @param Timeline $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Timeline $entity)
    {
        $form = $this->createForm(new TimelineType(), $entity, array(
            'action' => $this->generateUrl('admin_timeline_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Timeline entity.
     *
     * @Route("/new", name="admin_timeline_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Timeline();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Timeline entity.
     *
     * @Route("/{id}", name="admin_timeline_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Timeline')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timeline entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Timeline entity.
     *
     * @Route("/{id}/edit", name="admin_timeline_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Timeline')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timeline entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Timeline entity.
    *
    * @param Timeline $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Timeline $entity)
    {
        $form = $this->createForm(new TimelineType(), $entity, array(
            'action' => $this->generateUrl('admin_timeline_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing Timeline entity.
     *
     * @Route("/{id}", name="admin_timeline_update")
     * @Method("PUT")
     * @Template("AdminBundle:Timeline:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Timeline')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Timeline entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_timeline_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Timeline entity.
     *
     * @Route("/{id}", name="admin_timeline_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Timeline')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Timeline entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_timeline'));
    }

    /**
     * Creates a form to delete a Timeline entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_timeline_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
