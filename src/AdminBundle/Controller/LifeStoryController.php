<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\LifeStory;
use AdminBundle\Form\LifeStoryType;

/**
 * LifeStory controller.
 *
 * @Route("/lifestory")
 */
class LifeStoryController extends Controller
{

    /**
     * Lists all LifeStory entities.
     *
     * @Route("/", name="admin_lifestory")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $count = 10;
        $page = max($request->get('page'), 1);

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdminBundle:LifeStory')->findBy([], ['id' => 'desc'], $count, ($count * ($page - 1)));

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new LifeStory entity.
     *
     * @Route("/", name="admin_lifestory_create")
     * @Method("POST")
     * @Template("AdminBundle:LifeStory:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new LifeStory();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if (is_object($entity->getPhoto()) &&
                get_class($entity->getPhoto()) == 'Symfony\Component\HttpFoundation\File\UploadedFile'
            ) {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadableManager->markEntityToUpload($entity, $entity->getPhoto());
            }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_lifestory_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a LifeStory entity.
     *
     * @param LifeStory $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(LifeStory $entity)
    {
        $form = $this->createForm(new LifeStoryType(), $entity, array(
            'action' => $this->generateUrl('admin_lifestory_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new LifeStory entity.
     *
     * @Route("/new", name="admin_lifestory_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new LifeStory();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a LifeStory entity.
     *
     * @Route("/{id}", name="admin_lifestory_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:LifeStory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LifeStory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing LifeStory entity.
     *
     * @Route("/{id}/edit", name="admin_lifestory_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:LifeStory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LifeStory entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a LifeStory entity.
    *
    * @param LifeStory $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(LifeStory $entity)
    {
        $form = $this->createForm(new LifeStoryType(), $entity, array(
            'action' => $this->generateUrl('admin_lifestory_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing LifeStory entity.
     *
     * @Route("/{id}", name="admin_lifestory_update")
     * @Method("PUT")
     * @Template("AdminBundle:LifeStory:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:LifeStory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LifeStory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if (is_object($entity->getPhoto()) &&
                get_class($entity->getPhoto()) == 'Symfony\Component\HttpFoundation\File\UploadedFile'
            ) {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadableManager->markEntityToUpload($entity, $entity->getPhoto());
            }

            $em->flush();

            return $this->redirect($this->generateUrl('admin_lifestory_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a LifeStory entity.
     *
     * @Route("/{id}", name="admin_lifestory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:LifeStory')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find LifeStory entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_lifestory'));
    }

    /**
     * Creates a form to delete a LifeStory entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_lifestory_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
