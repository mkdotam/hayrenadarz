<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\NewsArticle;
use AdminBundle\Form\NewsArticleType;

/**
 * NewsArticle controller.
 *
 * @Route("/newsarticle")
 */
class NewsArticleController extends Controller
{

    /**
     * Lists all NewsArticle entities.
     *
     * @Route("/", name="admin_newsarticle")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $count = 10;
        $page = max($request->get('page'), 1);

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdminBundle:NewsArticle')->findBy([], ['id' => 'desc'], $count, ($count * ($page - 1)));

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new NewsArticle entity.
     *
     * @Route("/", name="admin_newsarticle_create")
     * @Method("POST")
     * @Template("AdminBundle:NewsArticle:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new NewsArticle();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (is_object($entity->getPdf()) &&
                get_class($entity->getPdf()) == 'Symfony\Component\HttpFoundation\File\UploadedFile') {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadableManager->markEntityToUpload($entity, $entity->getPdf());
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_newsarticle_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a NewsArticle entity.
     *
     * @param NewsArticle $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(NewsArticle $entity)
    {
        $form = $this->createForm(new NewsArticleType(), $entity, array(
            'action' => $this->generateUrl('admin_newsarticle_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new NewsArticle entity.
     *
     * @Route("/new", name="admin_newsarticle_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new NewsArticle();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a NewsArticle entity.
     *
     * @Route("/{id}", name="admin_newsarticle_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:NewsArticle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsArticle entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing NewsArticle entity.
     *
     * @Route("/{id}/edit", name="admin_newsarticle_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:NewsArticle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsArticle entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a NewsArticle entity.
    *
    * @param NewsArticle $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(NewsArticle $entity)
    {
        $form = $this->createForm(new NewsArticleType(), $entity, array(
            'action' => $this->generateUrl('admin_newsarticle_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing NewsArticle entity.
     *
     * @Route("/{id}", name="admin_newsarticle_update")
     * @Method("PUT")
     * @Template("AdminBundle:NewsArticle:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:NewsArticle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsArticle entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if (is_object($entity->getPdf()) &&
                get_class($entity->getPdf()) == 'Symfony\Component\HttpFoundation\File\UploadedFile') {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadableManager->markEntityToUpload($entity, $entity->getPdf());
            }
            $em->flush();

            return $this->redirect($this->generateUrl('admin_newsarticle_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a NewsArticle entity.
     *
     * @Route("/{id}", name="admin_newsarticle_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:NewsArticle')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NewsArticle entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_newsarticle'));
    }

    /**
     * Creates a form to delete a NewsArticle entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_newsarticle_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
