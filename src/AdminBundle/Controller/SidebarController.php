<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Sidebar;
use AdminBundle\Form\SidebarType;

/**
 * Sidebar controller.
 *
 * @Route("/sidebar")
 */
class SidebarController extends Controller
{

    /**
     * Lists all Sidebar entities.
     *
     * @Route("/", name="admin_sidebar")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $count = 10;
        $page = max($request->get('page'), 1);

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AdminBundle:Sidebar')->findBy([], ['id' => 'desc'], $count, ($count * ($page - 1)));

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Sidebar entity.
     *
     * @Route("/", name="admin_sidebar_create")
     * @Method("POST")
     * @Template("AdminBundle:Sidebar:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Sidebar();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_sidebar_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Sidebar entity.
     *
     * @param Sidebar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Sidebar $entity)
    {
        $form = $this->createForm(new SidebarType(), $entity, array(
            'action' => $this->generateUrl('admin_sidebar_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Sidebar entity.
     *
     * @Route("/new", name="admin_sidebar_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Sidebar();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Sidebar entity.
     *
     * @Route("/{id}", name="admin_sidebar_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Sidebar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sidebar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Sidebar entity.
     *
     * @Route("/{id}/edit", name="admin_sidebar_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Sidebar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sidebar entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Sidebar entity.
    *
    * @param Sidebar $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Sidebar $entity)
    {
        $form = $this->createForm(new SidebarType(), $entity, array(
            'action' => $this->generateUrl('admin_sidebar_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Sidebar entity.
     *
     * @Route("/{id}", name="admin_sidebar_update")
     * @Method("PUT")
     * @Template("AdminBundle:Sidebar:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Sidebar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sidebar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_sidebar_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Sidebar entity.
     *
     * @Route("/{id}", name="admin_sidebar_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Sidebar')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sidebar entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_sidebar'));
    }

    /**
     * Creates a form to delete a Sidebar entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_sidebar_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
