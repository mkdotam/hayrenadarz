<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RepatriationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('case')
            ->add('firstName')
            ->add('secondName')
            ->add('lastName')
            ->add('familyStatus')
            ->add('familyMemberCount')
            ->add('birthYear')
            ->add('inYear')
            ->add('ship')
            ->add('fromCity')
            ->add('fromCountry')
            ->add('toPlace')
            ->add('profession')
            ->add('other')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\Repatriation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_repatriation';
    }
}
