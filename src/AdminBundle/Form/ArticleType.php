<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AdminBundle\Entity\Article;

class ArticleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', null, ['required' => false])
            ->add('title')
            ->add('annotation')
            ->add('text')
            ->add('isPublished')
            ->add('author')
            ->add('issuu')
            ->add('file', 'file', ['required' => false, 'data_class' => null, 'mapped' => true])
            ->add('tag')
            ->add('type', 'choice', array('choices' => Article::getTypes()))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\Article'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_article';
    }
}
