<?php

namespace AdminBundle\Form;

use AdminBundle\Entity\Timeline;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimelineType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', null, ['required' => false])
            ->add('title')
            ->add('description')
            ->add('peopleCount')
            ->add('yearFrom')
            ->add('yearTo')
            ->add('type', 'choice', [
                'choices' => Timeline::$types,
                'expanded' => true
            ])
            ->add('locationFrom')
            ->add('locationTo')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\Timeline'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_timeline';
    }
}
