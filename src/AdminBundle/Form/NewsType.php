<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', null, ['required' => false])
            ->add('title')
            ->add('lead')
            ->add('body')
            ->add('photo', 'file', ['required' => false, 'data_class' => null, 'mapped' => true])
            ->add('isPublished', null, ['required' => false])
            ->add('created', 'datetime', array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr'   => array('class' => 'datepicker')))
            ->add('album')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\News'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_news';
    }
}
