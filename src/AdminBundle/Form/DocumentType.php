<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AdminBundle\Form\FileType;

class DocumentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', null, ['required' => false])
            ->add('title')
            ->add('annotation')
            ->add('year')
            ->add('isPublished')
            ->add('issuu')
            ->add('file', 'file', ['required' => false, 'empty_data' => '', 'data_class' => null, 'mapped' => true])
            ->add('link')
            ->add('person')
            ->add('tag')
            ->add('topic')
            ->add('translation');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AdminBundle\Entity\Document'
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_document';
    }
}
