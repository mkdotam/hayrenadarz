<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LifeStoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', null, ['required' => false])
            ->add('name')
            ->add('intro')
            ->add('bio')
            ->add('quote')
            ->add('photo', 'file', ['required' => false, 'data_class' => null, 'mapped' => true])
            ->add('video')
            ->add('audio')
            ->add('links', 'bootstrap_collection', array(
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'required' => false))
            ->add('remarkable')
            ->add('outlook')
            ->add('album')
            ->add('docs')
            ->add('newsarticles')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\LifeStory'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_lifestory';
    }
}
