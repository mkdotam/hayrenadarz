<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 12/13/15
 * Time: 12:25
 */

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * History
 *
 * @ORM\Entity
 */
class History extends PageProto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="History")
     * @ORM\JoinColumn(name="parentPage", referencedColumnName="id", nullable=true)
     */
    private $parentPage;


    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Video", inversedBy="history")
     * @ORM\JoinColumn(name="video", referencedColumnName="id", nullable=true)
     */
    private $video;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;


    /**
     * Add video
     *
     * @param \AdminBundle\Entity\Video $video
     * @return History
     */
    public function addVideo(\AdminBundle\Entity\Video $video)
    {
        $this->video[] = $video;

        return $this;
    }

    /**
     * Remove video
     *
     * @param \AdminBundle\Entity\Video $video
     */
    public function removeVideo(\AdminBundle\Entity\Video $video)
    {
        $this->video->removeElement($video);
    }

    /**
     * Get video
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set parentPage
     *
     * @param \AdminBundle\Entity\History $parentPage
     * @return History
     */
    public function setParentPage(\AdminBundle\Entity\History $parentPage = null)
    {
        $this->parentPage = $parentPage;

        return $this;
    }

    /**
     * Get parentPage
     *
     * @return \AdminBundle\Entity\History 
     */
    public function getParentPage()
    {
        return $this->parentPage;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return History
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }
}
