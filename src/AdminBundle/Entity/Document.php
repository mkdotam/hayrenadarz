<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Document
 *
 * @ORM\Entity
 */
class Document extends DocumentProto
{
    private $file;
    
    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Topic", inversedBy="docs")
     * @ORM\JoinTable(name="document_topic")
     */
    private $topic;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="translation", type="text", nullable=true)
     */
    private $translation;

    /**
     * @var string
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * Set year
     *
     * @param integer $year
     * @return Document
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Add topic
     *
     * @param \AdminBundle\Entity\Topic $topic
     * @return Document
     */
    public function addTopic(\AdminBundle\Entity\Topic $topic)
    {
        $this->topic[] = $topic;

        return $this;
    }

    /**
     * Remove topic
     *
     * @param \AdminBundle\Entity\Topic $topic
     */
    public function removeTopic(\AdminBundle\Entity\Topic $topic)
    {
        $this->topic->removeElement($topic);
    }

    /**
     * Get topic
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set translation
     *
     * @param string $translation
     * @return Document
     */
    public function setTranslation($translation)
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Get translation
     *
     * @return string
     */
    public function getTranslation()
    {
        return $this->translation;
    }
}
