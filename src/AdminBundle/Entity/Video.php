<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * Video
 *
 * @ORM\Table(name="video")
 * @ORM\Entity(repositoryClass="AdminBundle\Entity\Repository\VideoRepository")
 * @Gedmo\Uploadable(
 *     path="up/document/",
 *     allowOverwrite=true,
 *     appendNumber=true,
 *     allowedTypes="image/jpeg,image/pjpeg,image/png,image/x-png"
 * )
 */
class Video
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Gedmo\UploadableFilePath
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     */
    private $file;
    
    /**
     * @var string
     *
     * @ORM\Column(name="linkToTube", type="string", length=255, nullable=true)
     */
    private $linkToTube;

    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinColumn(name="tags", referencedColumnName="id", nullable=true)
     */
    private $tags;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isPublished", type="boolean", nullable=true)
     */
    private $isPublished;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="date")
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @ORM\ManyToMany(targetEntity="History", mappedBy="video")
     */
    private $history;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    public static function getTypes()
    {
        $types = array(
            'lifestory' => 'Մարդկային պատմություն',
            'outlook' => 'Հայացք դրսից',
            'scientist' => 'Գիտնականներ'
        );
        return $types;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function __construct()
    {
        $this->isPublished = true;
        $this->createdAt = new \DateTime();
        $this->publishedAt = new \DateTime();
        $this->tags = new ArrayCollection();
    }

    /**
     * Set linkToTube
     *
     * @param string $linkToTube
     * @return Video
     */
    public function setLinkToTube($linkToTube)
    {
        $this->linkToTube = $linkToTube;

        return $this;
    }

    /**
     * Get linkToTube
     *
     * @return string
     */
    public function getLinkToTube()
    {
        $video = $this->linkToTube;
        $result = "";

        if (strpos($video , 'youtu')) {
            $result = "http://www.youtube.com/embed/" . substr($video , strrpos($video , '/'));
        } else if (strpos($video , 'vimeo')) {
            $result = "http://player.vimeo.com/video" . substr($video , strrpos($video , '/'));
        }

        if ($result) {
            return $result;
        }

        return $video;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Video
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     *
     * @return Video
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Video
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Video
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add tag
     *
     * @param \AdminBundle\Entity\Tag $tag
     *
     * @return Video
     */
    public function addTag(\AdminBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AdminBundle\Entity\Tag $tag
     */
    public function removeTag(\AdminBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Video
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Video
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add history
     *
     * @param \AdminBundle\Entity\History $history
     * @return Video
     */
    public function addHistory(\AdminBundle\Entity\History $history)
    {
        $this->history[] = $history;

        return $this;
    }

    /**
     * Remove history
     *
     * @param \AdminBundle\Entity\History $history
     */
    public function removeHistory(\AdminBundle\Entity\History $history)
    {
        $this->history->removeElement($history);
    }

    /**
     * Get history
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Video
     */
    public function setFile($file)
    {
        if ($file) {
            $this->file = $file;    
        }
        
        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }
}
