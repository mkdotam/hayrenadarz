<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LifeStory
 *
 * @ORM\Table()
 * @ORM\Entity
 * @Gedmo\Uploadable(
 *     path="up/lifestory",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg,image/png,image/x-png,application/pdf"
 * )
 */
class LifeStory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="intro", type="text")
     */
    private $intro;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="bio", type="text")
     */
    private $bio;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="quote", type="string", length=255)
     */
    private $quote;

    /**
     * @var string
     *
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg", "image/png", "image/x-png"}
     * )
     * @ORM\Column(name="photo", type="string", length=255)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="audio", type="string", length=255, nullable=true)
     */
    private $audio;

    /**
     * @var array
     *
     * @ORM\Column(name="links", type="json_array", nullable=true)
     */
    private $links;

    /**
     * @var boolean
     *
     * @ORM\Column(name="remarkable", type="boolean", nullable=true)
     */
    private $remarkable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="outlook", type="boolean", nullable=true)
     */
    private $outlook;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="date")
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @var array
     *
     * @ORM\ManyToOne(targetEntity="Album")
     * @ORM\JoinColumn(name="album", referencedColumnName="id", nullable=true)
     */
    private $album;

    /**
     * @var array
     *
     * @ORM\OneToOne(targetEntity="Video")
     * @ORM\JoinColumn(name="video", referencedColumnName="id", nullable=true)
     */
    private $video;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="DocumentProto", inversedBy="person")
     * @ORM\JoinColumn(name="docs_id", nullable=true)
     */
    private $docs;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="NewsArticle", inversedBy="person")
     * @ORM\JoinColumn(name="newsarticles_id", nullable=true)
     */
    private $newsarticles;

    public function __toString()
    {
        return $this->getName();
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->docs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return LifeStory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LifeStory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set intro
     *
     * @param string $intro
     * @return LifeStory
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;

        return $this;
    }

    /**
     * Get intro
     *
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set bio
     *
     * @param string $bio
     * @return LifeStory
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Set quote
     *
     * @param string $quote
     * @return LifeStory
     */
    public function setQuote($quote)
    {
        $this->quote = $quote;

        return $this;
    }

    /**
     * Get quote
     *
     * @return string
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto($photo)
    {
        if ($photo) {
            $this->photo = $photo;
        }

        return $this;
    }

    /**
     * Set audio
     *
     * @param string $audio
     * @return LifeStory
     */
    public function setAudio($audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return string
     */
    public function getAudio()
    {
        return $this->audio;
    }

    /**
     * Set links
     *
     * @param array $links
     * @return LifeStory
     */
    public function setLinks($links)
    {
        $this->links = $links;

        return $this;
    }

    /**
     * Get links
     *
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Set remarkable
     *
     * @param boolean $remarkable
     * @return LifeStory
     */
    public function setRemarkable($remarkable)
    {
        $this->remarkable = $remarkable;

        return $this;
    }

    /**
     * Get remarkable
     *
     * @return boolean
     */
    public function getRemarkable()
    {
        return $this->remarkable;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return LifeStory
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return LifeStory
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set album
     *
     * @param \AdminBundle\Entity\Album $album
     * @return LifeStory
     */
    public function setAlbum(\AdminBundle\Entity\Album $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \AdminBundle\Entity\Album
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * Add docs
     *
     * @param \AdminBundle\Entity\DocumentProto $docs
     * @return LifeStory
     */
    public function addDoc(\AdminBundle\Entity\DocumentProto $docs)
    {
        $this->docs[] = $docs;

        return $this;
    }

    /**
     * Remove docs
     *
     * @param \AdminBundle\Entity\DocumentProto $docs
     */
    public function removeDoc(\AdminBundle\Entity\DocumentProto $docs)
    {
        $this->docs->removeElement($docs);
    }

    /**
     * Get docs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocs()
    {
        return $this->docs;
    }

    /**
     * Add newsarticles
     *
     * @param \AdminBundle\Entity\NewsArticle $newsarticles
     * @return LifeStory
     */
    public function addNewsarticle(\AdminBundle\Entity\NewsArticle $newsarticles)
    {
        $this->newsarticles[] = $newsarticles;

        return $this;
    }

    /**
     * Remove newsarticles
     *
     * @param \AdminBundle\Entity\NewsArticle $newsarticles
     */
    public function removeNewsarticle(\AdminBundle\Entity\NewsArticle $newsarticles)
    {
        $this->newsarticles->removeElement($newsarticles);
    }

    /**
     * Get newsarticles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewsarticles()
    {
        return $this->newsarticles;
    }

    /**
     * Set outlook
     *
     * @param boolean $outlook
     * @return LifeStory
     */
    public function setOutlook($outlook)
    {
        $this->outlook = $outlook;

        return $this;
    }

    /**
     * Get outlook
     *
     * @return boolean
     */
    public function getOutlook()
    {
        return $this->outlook;
    }

    /**
     * Set video
     *
     * @param \AdminBundle\Entity\Video $video
     * @return LifeStory
     */
    public function setVideo(\AdminBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \AdminBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }
}
