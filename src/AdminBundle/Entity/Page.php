<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 12/13/15
 * Time: 12:25
 */

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Page
 *
 * @ORM\Entity
 */
class Page extends PageProto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumn(name="parentPage", referencedColumnName="id", nullable=true)
     */
    private $parentPage;



    /**
     * Set parentPage
     *
     * @param \AdminBundle\Entity\Page $parentPage
     * @return Page
     */
    public function setParentPage(\AdminBundle\Entity\Page $parentPage = null)
    {
        $this->parentPage = $parentPage;

        return $this;
    }

    /**
     * Get parentPage
     *
     * @return \AdminBundle\Entity\Page 
     */
    public function getParentPage()
    {
        return $this->parentPage;
    }
}
