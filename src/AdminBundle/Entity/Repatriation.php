<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Repatriation
 *
 * @ORM\Table(indexes={@ORM\Index(columns={"firstName", "lastName", "case", "inYear"})})
 * @ORM\Entity(repositoryClass="AdminBundle\Entity\Repository\RepatriationRepository")
 */
class Repatriation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="case", type="string", length=255)
     */
    private $case;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="secondName", type="string", length=255)
     */
    private $secondName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="familyStatus", type="string", length=4)
     */
    private $familyStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="familyMemberCount", type="integer")
     */
    private $familyMemberCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="birthdate", type="integer")
     */
    private $birthYear;

    /**
     * @var integer
     *
     * @ORM\Column(name="inYear", type="integer")
     */
    private $inYear;

    /**
     * @var string
     *
     * @ORM\Column(name="ship", type="string", length=255)
     */
    private $ship;

    /**
     * @var string
     *
     * @ORM\Column(name="fromCity", type="string", length=255)
     */
    private $fromCity;

    /**
     * @var string
     *
     * @ORM\Column(name="fromCountry", type="string", length=255)
     */
    private $fromCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="toPlace", type="string", length=255)
     */
    private $toPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="profession", type="string", length=255)
     */
    private $profession;

    /**
     * @var string
     *
     * @ORM\Column(name="other", type="string", length=255)
     */
    private $other;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="date")
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;


    private $show;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCase()
    {
        return $this->case;
    }

    /**
     * @param int $case
     */
    public function setCase($case)
    {
        $this->case = $case;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Repatriation
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set secondName
     *
     * @param string $secondName
     * @return Repatriation
     */
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * Get secondName
     *
     * @return string 
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Repatriation
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set familyStatus
     *
     * @param string $familyStatus
     * @return Repatriation
     */
    public function setFamilyStatus($familyStatus)
    {
        $this->familyStatus = $familyStatus;

        return $this;
    }

    /**
     * Get familyStatus
     *
     * @return string 
     */
    public function getFamilyStatus()
    {
        return $this->familyStatus;
    }

    /**
     * Set familyMemberCount
     *
     * @param integer $familyMemberCount
     * @return Repatriation
     */
    public function setFamilyMemberCount($familyMemberCount)
    {
        $this->familyMemberCount = $familyMemberCount;

        return $this;
    }

    /**
     * Get familyMemberCount
     *
     * @return integer 
     */
    public function getFamilyMemberCount()
    {
        return $this->familyMemberCount;
    }

    /**
     * Set birthYear
     *
     * @param integer $birthYear
     * @return Repatriation
     */
    public function setBirthYear($birthYear)
    {
        $this->birthYear = $birthYear;

        return $this;
    }

    /**
     * Get birthYear
     *
     * @return integer 
     */
    public function getBirthYear()
    {
        return $this->birthYear;
    }

    /**
     * Set inYear
     *
     * @param integer $inYear
     * @return Repatriation
     */
    public function setInYear($inYear)
    {
        $this->inYear = $inYear;

        return $this;
    }

    /**
     * Get inYear
     *
     * @return integer 
     */
    public function getInYear()
    {
        return $this->inYear;
    }

    /**
     * Set ship
     *
     * @param string $ship
     * @return Repatriation
     */
    public function setShip($ship)
    {
        $this->ship = $ship;

        return $this;
    }

    /**
     * Get ship
     *
     * @return string 
     */
    public function getShip()
    {
        return $this->ship;
    }

    /**
     * Set fromCity
     *
     * @param string $fromCity
     * @return Repatriation
     */
    public function setFromCity($fromCity)
    {
        $this->fromCity = $fromCity;

        return $this;
    }

    /**
     * Get fromCity
     *
     * @return string 
     */
    public function getFromCity()
    {
        return $this->fromCity;
    }

    /**
     * Set fromCountry
     *
     * @param string $fromCountry
     * @return Repatriation
     */
    public function setFromCountry($fromCountry)
    {
        $this->fromCountry = $fromCountry;

        return $this;
    }

    /**
     * Get fromCountry
     *
     * @return string 
     */
    public function getFromCountry()
    {
        return $this->fromCountry;
    }

    /**
     * Set toPlace
     *
     * @param string $toPlace
     * @return Repatriation
     */
    public function setToPlace($toPlace)
    {
        $this->toPlace = $toPlace;

        return $this;
    }

    /**
     * Get toPlace
     *
     * @return string 
     */
    public function getToPlace()
    {
        return $this->toPlace;
    }

    /**
     * Set profession
     *
     * @param string $profession
     * @return Repatriation
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string 
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set other
     *
     * @param string $other
     * @return Repatriation
     */
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return string 
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @return mixed
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param mixed $show
     */
    public function setShow($show)
    {
        $this->show = $show;
    }

    /**
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    

}
