<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Book
 *
 * @ORM\Entity
 */
class Book extends DocumentProto
{
    private $file;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="publishedBy", type="string", length=255, nullable=true)
     */
    private $publishedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="ISBN", type="string", length=255, nullable=true)
     */
    private $ISBN;

    /**
     * @var string
     *
     * @ORM\Column(name="pageCount", type="string", length=255, nullable=true)
     */
    private $pageCount;

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Book
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set publishedBy
     *
     * @param string $publishedBy
     * @return Book
     */
    public function setPublishedBy($publishedBy)
    {
        $this->publishedBy = $publishedBy;

        return $this;
    }

    /**
     * Get publishedBy
     *
     * @return string 
     */
    public function getPublishedBy()
    {
        return $this->publishedBy;
    }

    /**
     * Set ISBN
     *
     * @param string $iSBN
     * @return Book
     */
    public function setISBN($iSBN)
    {
        $this->ISBN = $iSBN;

        return $this;
    }

    /**
     * Get ISBN
     *
     * @return string 
     */
    public function getISBN()
    {
        return $this->ISBN;
    }

    /**
     * Set pageCount
     *
     * @param string $pageCount
     * @return Book
     */
    public function setPageCount($pageCount)
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    /**
     * Get pageCount
     *
     * @return string 
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }
}
