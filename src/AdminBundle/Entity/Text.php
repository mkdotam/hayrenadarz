<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Text
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Text
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="textID", type="string", length=255)
     */
    private $textID;

    public static function getIds()
    {
        return [
            0 => 'home 0',
            1 => 'home 1',
            2 => 'home 2',
            21 => 'home 21',
            22 => 'home 22',
            23 => 'home 23',
            24 => 'home 24',
            25 => 'home 25',
            3 => 'home 3',
            4 => 'home 4',
            5 => 'home 5',
            6 => 'home 6',
            7 => 'home 7',
        ];
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Text
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Text
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set textID
     *
     * @param string $textID
     * @return Text
     */
    public function setTextID($textID)
    {
        $this->textID = $textID;

        return $this;
    }

    /**
     * Get textID
     *
     * @return string 
     */
    public function getTextID()
    {
        return $this->textID;
    }
}
