<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Timeline
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Timeline
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"}, updatable=false)
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="peopleCount", type="integer")
     */
    private $peopleCount;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="locationFrom", referencedColumnName="id", nullable=true)
     */
    private $locationFrom;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="locationTo", referencedColumnName="id", nullable=true)
     */
    private $locationTo;

    /**
     * @var integer
     *
     * @ORM\Column(name="yearFrom", type="integer")
     */
    private $yearFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="yearTo", type="integer")
     */
    private $yearTo;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="date")
     */
    private $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    public static $types = [
        'nergaght' => 'Ներգաղթ',
        'brnachnshumner' => 'Բռնաճնշումներ'
    ];

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Timeline
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Timeline
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Timeline
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set peopleCount
     *
     * @param integer $peopleCount
     * @return Timeline
     */
    public function setPeopleCount($peopleCount)
    {
        $this->peopleCount = $peopleCount;

        return $this;
    }

    /**
     * Get peopleCount
     *
     * @return integer 
     */
    public function getPeopleCount()
    {
        return $this->peopleCount;
    }

    /**
     * Set yearFrom
     *
     * @param integer $yearFrom
     * @return Timeline
     */
    public function setYearFrom($yearFrom)
    {
        $this->yearFrom = $yearFrom;

        return $this;
    }

    /**
     * Get yearFrom
     *
     * @return integer 
     */
    public function getYearFrom()
    {
        return $this->yearFrom;
    }

    /**
     * Set yearTo
     *
     * @param integer $yearTo
     * @return Timeline
     */
    public function setYearTo($yearTo)
    {
        $this->yearTo = $yearTo;

        return $this;
    }

    /**
     * Get yearTo
     *
     * @return integer 
     */
    public function getYearTo()
    {
        return $this->yearTo;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Timeline
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Timeline
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Timeline
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set locationFrom
     *
     * @param \AdminBundle\Entity\Location $locationFrom
     * @return Timeline
     */
    public function setLocationFrom(\AdminBundle\Entity\Location $locationFrom = null)
    {
        $this->locationFrom = $locationFrom;

        return $this;
    }

    /**
     * Get locationFrom
     *
     * @return \AdminBundle\Entity\Location 
     */
    public function getLocationFrom()
    {
        return $this->locationFrom;
    }

    /**
     * Set locationTo
     *
     * @param \AdminBundle\Entity\Location $locationTo
     * @return Timeline
     */
    public function setLocationTo(\AdminBundle\Entity\Location $locationTo = null)
    {
        $this->locationTo = $locationTo;

        return $this;
    }

    /**
     * Get locationTo
     *
     * @return \AdminBundle\Entity\Location 
     */
    public function getLocationTo()
    {
        return $this->locationTo;
    }
}
