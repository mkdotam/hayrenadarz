<?php

namespace AdminBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Proxies\__CG__\AdminBundle\Entity\Repatriation;

/**
 * RepatriationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RepatriationRepository extends EntityRepository
{
    public function search($lastName, $firstName = null, $year = null)
    {
        $lastName = trim($lastName);
        $firstName = trim($firstName);
        $year = (int)trim($year);

        $em = $this->getEntityManager();
        $sql = "SELECT DISTINCT r.case FROM AdminBundle:Repatriation as r
                                    WHERE r.lastName like '%" . $lastName . "%' ";

        if ($firstName) {
            $sql .= " AND r.firstName like '%" . $firstName . "%' ";
        }
        if ($year) {
            $sql .= " AND r.inYear like '%" . $year. "%' ";
        }
        $sql .= "order by r.id, r.case";

        $query = $em->createQuery($sql);
        $ids = $query->getResult();

        $result = [];
        foreach ($ids as $id) {
            $i = $id['case'];
            $query = $em->createQuery("SELECT r FROM AdminBundle:Repatriation as r WHERE r.case = '".$i."'");
            $family = $query->getResult();

            $result[$i] = [];
            $other = [];
            $same = [];
            /** @var Repatriation $member */
            foreach ($family as $member) {
                $found = strstr($member->getLastName(), $lastName);
                if ($found && !empty($firstName)) {
                    $found = strstr($member->getFirstName(), $firstName);
                }
                if ($found) {
                    $member->setShow(true);
                } else {
                    $member->setShow(false);
                }
                $result[$i][] = $member;
            }
            uasort(
                $result[$i],
                function ($a, $b) {
                    return ($a->getShow() > $b->getShow()) ? -1 : 1;
                }
            );
        }

        return $result;
    }
}
