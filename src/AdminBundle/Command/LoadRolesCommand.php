<?php

namespace AdminBundle\Command;

use AdminBundle\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadRolesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('mk:load:roles')
            ->setDescription('Load all roles.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $roleRepo = $em->getRepository('AdminBundle:Role');

        $availableRoles = $roleRepo->findAll();
        if (!$availableRoles) {

            $output->writeln("Generating roles:");

            $roles = [
                'admin' => 'ROLE_ADMIN',
                'user'  => 'ROLE_USER'
            ];

            foreach ($roles as $name => $role) {

                $r = new Role();
                $r->setName($name);
                $r->setRole($role);

                $em->persist($r);
                $output->writeln(" - ".$name);

            }
            $em->flush();
        } else {
            $output->writeln("Roles already generated.");
        }

    }
}
