<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 10/12/15
 * Time: 12:09
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/lifestory")
 */
class LifeStoryController extends Controller
{

    /**
     * @Route("/all", name="lifestories")
     */
    public function lifestoriesAction(Request $request)
    {
        $em = $this->getDoctrine();
        $stories = $em->getRepository('AdminBundle:LifeStory')->findBy(['outlook' => 0, 'remarkable' => 0], ['created' => 'desc']);

        return $this->render(
            'AppBundle:lifestory:lifestories.html.twig',
            [
                'stories' => $stories
            ]
        );
    }

    /**
     * @Route("/remarkable", name="remarkable")
     */
    public function remarkableAction(Request $request)
    {
        $em = $this->getDoctrine();
        $stories = $em->getRepository('AdminBundle:LifeStory')->findBy(['remarkable' => 1], ['created' => 'desc']);

        return $this->render(
            'AppBundle:lifestory:remarkable.html.twig',
            [
                'stories' => $stories
            ]
        );
    }

    /**
     * @Route("/outlook", name="outlook")
     */
    public function outlookAction(Request $request)
    {
        $em = $this->getDoctrine();
        $stories = $em->getRepository('AdminBundle:LifeStory')->findBy(['outlook' => 1], ['created' => 'desc']);

        return $this->render(
            'AppBundle:lifestory:outlook.html.twig',
            [
                'stories' => $stories
            ]
        );
    }

    /**
     * @Route("/{slug}", name="lifestory")
     */
    public function lifestoryAction($slug)
    {
        $em = $this->getDoctrine();
        $story = $em->getRepository('AdminBundle:LifeStory')->findOneBySlug($slug);

        return $this->render(
            'AppBundle:lifestory:lifestory.html.twig',
            [
                'i' => $story
            ]
        );
    }
}
