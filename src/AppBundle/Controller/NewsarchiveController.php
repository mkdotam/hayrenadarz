<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 10/26/15
 * Time: 11:24
 */

namespace AppBundle\Controller;

use AdminBundle\Entity\Newspaper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/newsarchive")
 */
class NewsarchiveController extends Controller
{

    /**
     * @Route("/", name="newsarchive")
     */
    public function archiveAction(Request $request)
    {
        $em = $this->getDoctrine();
        $location = $request->get('location');
        $locations = $em->getRepository('AdminBundle:Location')->findAll();
        sort($locations);

        if ($location) {
            $qb = $em->getManager()->createQueryBuilder();
            $query = $qb->select('n')
                ->from('AdminBundle:Newspaper', 'n')
                ->leftJoin('n.location', 'l')
                ->where(" 1 = 1 ");
            if ($location) {
                $query->andWhere('l.title = :location')
                    ->setParameter('location', $location);
            }

            $newspapers = $query->getQuery()->getResult();
        } else {
            $newspapers = $em->getRepository('AdminBundle:Newspaper')->findBy([], ['title' => 'asc']);
        }

        return $this->render(
            'AppBundle:vault:newsarchive.html.twig',
            [
                'newspapers' => $newspapers,
                'locations'  => $locations,
                'location'   => $location
            ]
        );
    }

    /**
     * @Route("/{slug}", name="newspaper")
     */
    public function newspaperAction($slug, Request $request)
    {
        $em = $this->getDoctrine();
        $newspaper = $em->getRepository('AdminBundle:Newspaper')->findOneBySlug($slug);
        $page = max($request->get('page'), 1);
        $count = 12;
        $start = ($count * ($page - 1));


        $qb = $em->getManager()->createQueryBuilder();
        $years = $qb->select('distinct YEAR(t.publicationDate)')
            ->from('AdminBundle:NewsArticle', 't')
            ->where('t.newspaper = :newspaper')
            ->setParameter('newspaper', $newspaper)
            ->getQuery()->getScalarResult();
        $years = (array_map('current', $years));
        sort($years);
        $year = $request->get('year');

        if ($year) {
            $qb = $em->getManager()->createQueryBuilder();
            $query = $qb->select('n')
                ->from('AdminBundle:NewsArticle', 'n')
                ->where(" 1 = 1 ")
                ->andWhere('n.newspaper = :newspaper')
                ->andWhere('YEAR(n.publicationDate) = :year')
                ->setFirstResult($start)
                ->setMaxResults($count)
                ->setParameter('newspaper', $newspaper)
                ->setParameter('year', $year);


            $articles = $query->getQuery()->getResult();
        } else {
            $articles = $em->getRepository('AdminBundle:NewsArticle')->findBy(
                ['newspaper' => $newspaper],
                ['publicationDate' => 'desc'],
                $count,
                $start
            );

        }


        return $this->render(
            'AppBundle:vault:newspaper.html.twig',
            [
                'i'        => $newspaper,
                'articles' => $articles,
                'years'    => $years,
                'year'     => $year

            ]
        );
    }

    /**
     * @Route("/article/{slug}", name="newsarticle")
     */
    public function articleAction($slug)
    {
        $em = $this->getDoctrine();
        $news = $em->getRepository('AdminBundle:NewsArticle')->findOneBySlug($slug);

        return $this->render(
            'AppBundle:vault:newsarticle.html.twig',
            [
                'i' => $news
            ]
        );
    }
}

