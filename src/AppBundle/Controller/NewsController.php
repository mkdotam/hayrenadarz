<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 10/12/15
 * Time: 12:09
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{

    /**
     * @Route("/news", name="news")
     */
    public function newsAction(Request $request)
    {
        $em = $this->getDoctrine();
        $page = max($request->get('page'), 1);
        $count = 12;
        $start = ($count * ($page - 1));

        $news = $em->getRepository('AdminBundle:News')->findBy(
            ['isPublished' => true],
            ['created' => 'desc'],
            $count , $start
        );

        return $this->render(
            'AppBundle:news:news.html.twig',
            [
                'news' => $news
            ]
        );
    }

    /**
     * @Route("/news/{slug}", name="anews")
     */
    public function anewsAction($slug)
    {
        $em = $this->getDoctrine();
        $aNews = $em->getRepository('AdminBundle:News')->findOneBySlug($slug);

        return $this->render(
            'AppBundle:news:anews.html.twig',
            [
                'i' => $aNews
            ]
        );
    }
}
