<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 10/12/15
 * Time: 12:09
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{

    /**
     * @Route("/articles", name="articles")
     */
    public function newsAction(Request $request)
    {
        $em = $this->getDoctrine();
        $page = max($request->get('page'), 1);
        $count = 12;
        $start = ($count * ($page - 1));

        $articles = $em->getRepository('AdminBundle:Article')->findBy(
            ['isPublished' => true],
            ['created' => 'desc'],
            $count , $start
        );

        return $this->render(
            'AppBundle:vault:articles.html.twig',
            [
                'articles' => $articles
            ]
        );
    }

    /**
     * @Route("/article/{slug}", name="article")
     */
    public function anewsAction($slug)
    {
        $em = $this->getDoctrine();
        $article = $em->getRepository('AdminBundle:Article')->findOneBySlug($slug);

        return $this->render(
            'AppBundle:vault:article.html.twig',
            [
                'i' => $article
            ]
        );
    }
}
