<?php

namespace AppBundle\Controller;

use AdminBundle\Entity\Sidebar;
use AdminBundle\Entity\Text;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $texts = $em->getRepository('AdminBundle:Text')->findAll();

        $res = [
            21 => null,
            22 => null,
            23 => null,
            24 => null,
            25 => null,
        ];
        /** @var Text $text */
        foreach ($texts as $text) {
            $res[$text->getTextID()] = $text;
        }

        // replace this example code with whatever you need
        return $this->render(
            'AppBundle:default:index.html.twig',
            [
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
                't'        => $res
            ]
        );
    }

    /**
     * @Route("/page/{slug}", name="page")
     */
    public function pageAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AdminBundle:Page')->findOneBy(['isPublished' => 1, 'slug' => $slug]);

        if (!$page) {
            return $this->redirect($this->generateUrl('home'));
        }

        return $this->render(
            'AppBundle:default:page.html.twig',
            [
                'page' => $page
            ]
        );
    }

    /**
     * @Route("/exhibiton", name="exhibition")
     */
    public function exhibitionAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render(
            'AppBundle:default:exhibition.html.twig',
            []
        );
    }

    /**
     * @Route("/banner/{tall}", name="banner")
     */
    public function bannerAction($tall = 0)
    {
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository('AdminBundle:Album')->findOneByType('banner');

        // replace this example code with whatever you need
        return $this->render(
            'AppBundle:partial:banner.html.twig',
            [
                'album' => $album,
                'tall'  => $tall
            ]
        );
    }

    /**
     * @Route("/sidebar/{type}", name="sidebar")
     */
    public function sidebarAction($type)
    {
        $em = $this->getDoctrine()->getManager();
        $sidebar = $em->getRepository('AdminBundle:Sidebar')->findOneByTitle($type);

        if (is_null($sidebar)) {
            $sidebar = new Sidebar();
            $sidebar->setTitle($type);
            $sidebar->setSlug($type);
            $em->persist($sidebar);
            $em->flush();
        }

        return $this->render(
            'AppBundle:partial:sidebar.html.twig',
            [
                'sidebar' => $sidebar
            ]
        );
    }


    /**
     * @Route("/pagination/{path}/{page}", name="pagination")
     */
    public function paginationAction($path, $page, ParameterBag $params = null, $query = null)
    {
        if (is_null($params)) {
            $params = [];
        }
        $em = $this->getDoctrine()->getManager();
        $page = max($page, 1);

        $qb = $em->createQueryBuilder();

        switch ($path) {
            case "newspaper":
                $newspaper = $em->getRepository('AdminBundle:Newspaper')->findOneBySlug($params->get('slug'));
                $q = $qb->select('count(t.id)')
                    ->from('AdminBundle:NewsArticle', 't')
                    ->where(" 1 = 1 ")
                    ->andWhere('t.newspaper = :newspaper')
                    ->setParameter('newspaper', $newspaper);
                if ($query->get('year')) {
                    $q->andWhere('YEAR(t.publicationDate) = :year')
                        ->setParameter('year', $query->get('year'));

                    $params->set('year', $query->get('year'));
                }
                $total = $q->getQuery()->getSingleScalarResult();
                $pages = ceil($total / 12);
                break;
            case "newspapers":
                $total = $qb->select('count(t.id)')
                    ->from('AdminBundle:Newspaper', 't')
                    //->where('t.isPublished = 1')
                    ->getQuery()->getSingleScalarResult();

                $pages = ceil($total / 12);
                break;
            case "news":
                $total = $qb->select('count(t.id)')
                    ->from('AdminBundle:News', 't')
                    ->where('t.isPublished = 1')
                    ->getQuery()->getSingleScalarResult();

                $pages = ceil($total / 12);
                break;
            case "books":
                $total = $qb->select('count(t.id)')
                    ->from('AdminBundle:Book', 't')
                    ->where('t.isPublished = 1')
                    ->getQuery()->getSingleScalarResult();

                $pages = ceil($total / 12);
                break;
            case "documents":
                $total = $qb->select('count(t.id)')
                    ->from('AdminBundle:Document', 't')
                    ->where('t.isPublished = 1')
                    ->getQuery()->getSingleScalarResult();

                $pages = ceil($total / 12);
                break;

            case "articles":
                $total = $qb->select('count(t.id)')
                    ->from('AdminBundle:Article', 't')
                    ->where('t.isPublished = 1')
                    ->getQuery()->getSingleScalarResult();

                $pages = ceil($total / 12);
                break;
            case "videoarchive":
                $total = $qb->select('count(t.id)')
                    ->from('AdminBundle:Video', 't')
                    ->where('t.isPublished = 1')
                    ->getQuery()->getSingleScalarResult();

                $pages = ceil($total / 12);
                break;
            case "photo":
                $total = $qb->select('count(t.id)')
                    ->from('AdminBundle:Album', 't')
                    ->getQuery()->getSingleScalarResult();

                $pages = ceil($total / 12);
                break;

            default:
                $total = 1;
                $pages = ceil($total / 12);
                break;
        }

        return $this->render(
            'AppBundle:partial:pagination.html.twig',
            [
                'path'   => $path,
                'pages'  => (int)$pages,
                'page'   => (int)$page,
                'params' => $params,
            ]
        );
    }

}
