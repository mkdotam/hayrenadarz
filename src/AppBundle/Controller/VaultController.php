<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 10/29/15
 * Time: 13:05
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class VaultController extends Controller
{


    /**
     * @Route("/pahoc", name="vault")
     */
    public function vaultAction()
    {
        $em = $this->getDoctrine();
        $newspaper = $em->getRepository('AdminBundle:Newspaper')->findOneBy([]);
        $book = $em->getRepository('AdminBundle:Book')->findOneBy([]);
        $document = $em->getRepository('AdminBundle:Document')->findOneBy([]);
        $article = $em->getRepository('AdminBundle:Article')->findOneBy([]);

        return $this->render(
            'AppBundle:vault:vault.html.twig',
            [
                'newspaper' => $newspaper,
                'book'      => $book,
                'document'  => $document,
                'article'   => $article,
            ]
        );
    }

    /**
     * @Route("/photoarchive", name="photoarchive")
     */
    public function photoarchiveAction(Request $request)
    {
        $em = $this->getDoctrine();
        $page = max($request->get('page'), 1);
        $count = 12;
        $start = ($count * ($page - 1));

        $albums = $em->getRepository('AdminBundle:Album')->findBy(
            ['type' => 'album'],
            ['id' => 'desc'],
            $count,
            $start
        );

        return $this->render(
            'AppBundle:vault:photoarchive.html.twig',
            [
                'albums' => $albums
            ]
        );
    }

    /**
     * @Route("/photostory/{slug}", name="photostory")
     */
    public function photoStoryAction($slug)
    {
        $em = $this->getDoctrine();
        $album = $em->getRepository('AdminBundle:Album')->findOneBySlug($slug);

        return $this->render(
            'AppBundle:vault:photostory.html.twig',
            [
                'a' => $album
            ]
        );
    }

    /**
     * @Route("/videoarchive", name="videoarchive")
     */
    public function videosAction(Request $request)
    {
        $count = 12;
        $page = max($request->get('page'), 1);

        $em = $this->getDoctrine()->getManager();
        $videos = $em->getRepository('AdminBundle:Video')->findBy(
            ['isPublished' => 1],
            ['id' => 'desc'],
            $count,
            ($count * ($page - 1))
        );

        return $this->render(
            'AppBundle:vault:videoarchive.html.twig',
            [
                'videos' => $videos
            ]
        );
    }

    /**
     * @Route("/video/{slug}", name="video")
     */
    public function videoAction($slug)
    {
        $em = $this->getDoctrine();
        $video = $em->getRepository('AdminBundle:Video')->findOneBySlug($slug);

        return $this->render(
            'AppBundle:vault:video.html.twig',
            [
                'i' => $video
            ]
        );
    }

}
