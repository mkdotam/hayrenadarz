<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 10/12/15
 * Time: 12:09
 */

namespace AppBundle\Controller;

use AdminBundle\Entity\Document;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DocumentController extends Controller
{

    /**
     * @Route("/documents", name="documents")
     */
    public function documentsAction(Request $request)
    {
        $em = $this->getDoctrine();
        $year = $request->get('year');
        $page = max($request->get('page'), 1);
        $count = 12;
        $start = ($count * ($page - 1));

        $qb = $em->getManager()->createQueryBuilder();
        $result = $qb->select('distinct t.year')
            ->from('AdminBundle:Document', 't')
            ->getQuery()->getScalarResult();
        $years = (array_map('current', $result));
        sort($years);

        if ($year) {
            $where ['year'] = $year;
        }
        $where['isPublished'] = true;
        $documents = $em->getRepository('AdminBundle:Document')->findBy(
            $where,
            ['created' => 'desc'],
            $count,
            $start
        );

        return $this->render(
            'AppBundle:vault:documents.html.twig',
            [
                'documents' => $documents,
                'years'     => $years,
                'year'      => $year
            ]
        );
    }

    /**
     * @Route("/document/{slug}", name="document")
     */
    public function documentAction($slug)
    {
        $em = $this->getDoctrine();
        /** @var Document $document */
        $document = $em->getRepository('AdminBundle:Document')->findOneBySlug($slug);
        $topic = $document->getTopic()->getValues();

        $qb = $em->getManager()->createQueryBuilder();
        $query = $qb->select('d')
            ->from('AdminBundle:Document', 'd')
            ->innerJoin('d.topic', 't')
            ->where('t.title = :t')
            ->setParameter('t', $topic[0]);
        ;
        $relatedDocuments = $query->getQuery()->getResult();

        return $this->render(
            'AppBundle:vault:document.html.twig',
            [
                'i' => $document,
                'related' => $relatedDocuments
            ]
        );
    }

}
