<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 12/11/15
 * Time: 22:21
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/history")
 */
class HistoryController extends Controller
{
    /**
     * @Route("/", name="history")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $album = $em->getRepository('AdminBundle:Album')->findOneByType('history');
        $pages = $em->getRepository('AdminBundle:History')->findByIsPublished(true);
        $page = $em->getRepository('AdminBundle:History')->findOneBy(['slug' => 'history', 'isPublished' => true]);

        return $this->render(
            'AppBundle:history:index.html.twig',
            [
                'album' => $album,
                'pages' => $pages,
                'page' => $page
            ]
        );
    }

    /**
     * @Route("/{slug}", name="history_slug")
     */
    public function historyAction($slug = 'history')
    {
        $em = $this->getDoctrine()->getManager();

        $album = $em->getRepository('AdminBundle:Album')->findOneByType('history');
        $pages = $em->getRepository('AdminBundle:History')->findByIsPublished(true);
        $page = $em->getRepository('AdminBundle:History')->findOneBy(['slug' => $slug, 'isPublished' => true]);

        return $this->render(
            'AppBundle:history:history.html.twig',
            [
                'album' => $album,
                'pages' => $pages,
                'page' => $page
            ]
        );
    }
}