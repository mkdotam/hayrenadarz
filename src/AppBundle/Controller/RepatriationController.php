<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RepatriationController extends Controller
{
    /**
     * @Route("/search", name="repat_search")
     */
    public function searchAction(Request $request)
    {
        if ($request->get('lastname') || $request->get('firstname')) {
            $em = $this->getDoctrine();
            $rs = $em->getRepository('AdminBundle:Repatriation')->search(
                $request->get('lastname'),
                $request->get('firstname'),
                $request->get('year')
            );
        } else {
            $rs = array();
        }


        // replace this example code with whatever you need
        return $this->render(
            'AppBundle:default:search.html.twig',
            array(
                'repats' => $rs
            )
        );
    }
}