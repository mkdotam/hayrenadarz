<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 10/12/15
 * Time: 12:09
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BookController extends Controller
{

    /**
     * @Route("/books", name="books")
     */
    public function newsAction(Request $request)
    {
        $em = $this->getDoctrine();
        $page = max($request->get('page'), 1);
        $count = 9;
        $start = ($count * ($page - 1));

        $books = $em->getRepository('AdminBundle:Book')->findBy(
            ['isPublished' => true],
            ['created' => 'desc'],
            $count,
            $start
        );

        return $this->render(
            'AppBundle:vault:books.html.twig',
            [
                'books' => $books
            ]
        );
    }

    /**
     * @Route("/book/{slug}", name="book")
     */
    public function anewsAction($slug)
    {
        $em = $this->getDoctrine();
        $book = $em->getRepository('AdminBundle:Book')->findOneBySlug($slug);

        return $this->render(
            'AppBundle:vault:book.html.twig',
            [
                'i' => $book
            ]
        );
    }
}
