<?php

namespace AppBundle\Twig;

class InitialExtension extends \Twig_Extension {

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('initial', array($this, 'initialFilter')),
        );
    }

    public function initialFilter($text)
    {
        //$firstWord = trim(substr($text, 0, strpos($text, ' ')));
        //$otherWords = trim(substr($text, strpos($text, ' '), strlen($text)));
        //
        //$result = "<span class='initial'>" . $firstWord . "</span> " . $otherWords ;
        $result = "<p class='initial'>" . $text . "</p> ";

        return $result;
    }

    public function getName()
    {
        return 'initial_extension';
    }
}
