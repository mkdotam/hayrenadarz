<?php

namespace AppBundle\Twig;

class InstanceOfExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('instanceOf', array($this, 'instanceOfFilter')),
        );
    }

    public function instanceOfFilter($text)
    {
        $classname = get_class($text);
        $classNamePure = substr($classname, strrpos($classname, '\\') + 1);
        return $classNamePure;
    }

    public function getName()
    {
        return 'instanceOf_extension';
    }
}
